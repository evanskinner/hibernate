
package hibernate;

import java.util.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class hibernate {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		List<tracker> newEntries = new LinkedList<tracker>();
		SessionFactory factory = new Configuration()
									.configure("hibernate.cfg.xml")
									.addAnnotatedClass(tracker.class)
									.buildSessionFactory();
		Session session = factory.getCurrentSession();
		
		try {
			int one = -1;
			while(one != 0) {
				System.out.println("\n\nCurrently adding a new entry...\n");
				newEntries.add(entry(scan));
				System.out.println("\nYou have been checked in successfully");
				int choice = -1;
				while (choice != 0) {
					System.out.print("\nCheck in another patient? (Y/N): ");
					String cont = scan.nextLine().toUpperCase();
					if(cont.contentEquals("Y") || cont.contentEquals("YES")) {
						choice = 0;
					}
					else if(cont.contentEquals("N") || cont.contentEquals("NO")) {
						one = 0;
						choice = 0;
					}
					else {
						System.err.println("\nInvalid response. Please try again.");
					}		
				}
			}
			session.beginTransaction();
			System.out.println("Saving new entry...");
			for (tracker patient : newEntries) {
				
				session.save(patient);
			}
			session.getTransaction().commit();
		}
		finally{
			
			factory.close();
			scan.close();
			System.out.println("Finished!");
		}

	}

	public static tracker entry(Scanner scan) {

		System.out.print("Enter patient's first name: ");
		String name = scan.nextLine();

		System.out.print("\nEnter patient's last name: ");
		String last = scan.nextLine();

		Boolean appointment = false;
		int choice = -1;

		while (choice != 0) {
			
			System.out.print("\nDo they have an appointment? (True/False): ");
			String app = scan.nextLine().toUpperCase();
			if(app.contentEquals("T") || app.contentEquals("TRUE")) {
				
				appointment = true;
				choice = 0;
			}
			else if(app.contentEquals("F") || app.contentEquals("FALSE")) {
				choice = 0;
			}
			else {
				System.err.println("\nInvalid response. Please try again.");
			}		
		}
		return new tracker(name, last, appointment);
	}
}
