
package hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tracker")
public class tracker {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "first_name")
	private String name;
	
	@Column(name = "last_name")
	private String last;
	
	@Column(name = "appointment")
	private boolean question;

	public tracker() {
		
	}

	public tracker(String name, String last, boolean question) {
		this.name = name;
		this.last = last;
		this.question = question;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLast() {
		return last;
	}

	public void setLast(String last) {
		this.last = last;
	}

	public boolean question() {
		return question;
	}

	public void setQuestion(boolean question) {
		this.question = question;
	}

	@Override
	public String toString() {
		return "tracker [id=" + id + ", name=" + name + ", last=" + last + ", question=" + question + "]";
	}
}
